# Driver SO #

### What is this repository for? ###

* Driver example using Raspi DAC board 

### How do I get set up? ###

There are 3 folders, with three different modules:

* hello_world:  LKM simple example
* interrupt: LKM with an interrupt example
* leds_device: LKM with a driver for leds device

Use: 

* *make* to build the module

* *make install*, to install de module

* *make uninstall*, to unsinstall

### Who do I talk to? ###
* Andrés Rodríguez (andres@uma.es)