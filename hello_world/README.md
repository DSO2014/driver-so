# Driver SO #

### What is this repository for? ###

* Driver example using Raspi DAC board 

### How do I get set up? ###

Use: 

* *make* to build the module

* *make install*, to install de module

* *make uninstall*, to unsinstall

Once installed, use:

dmesg | tail               to see kernel messages

### Who do I talk to? ###
* Andrés Rodríguez (andres@uma.es)
