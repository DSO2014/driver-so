# Driver SO #

### What is this repository for? ###

* Driver example using Raspi DAC board 

### How do I get set up? ###

Use: 

* *make* to build the module

* *make install*, to install de module

* *make uninstall*, to unsinstall

Once installed, use:

* push button #1 in the board to fire an interrupt and move the led values
* cat /proc/interrupts     to see registered interrupts and counters

### Who do I talk to? ###
* Andrés Rodríguez (andres@uma.es)
